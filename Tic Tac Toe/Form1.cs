﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tic_Tac_Toe
{
    public partial class Form1 : Form
    {
        int[,] A;
        int pas;
        char J;
        public Form1()
        {
            InitializeComponent();
        }

        private void iesireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        void reset()
        {
            A = new int[3, 3];
            pas = 0;
            J = 'X';
            button1.Text = "";
            button2.Text = "";
            button3.Text = "";
            button4.Text = "";
            button5.Text = "";
            button6.Text = "";
            button7.Text = "";
            button8.Text = "";
            button9.Text = "";
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;
            button1.BackColor = Color.White;
            button2.BackColor = Color.White;
            button3.BackColor = Color.White;
            button4.BackColor = Color.White;
            button5.BackColor = Color.White;
            button6.BackColor = Color.White;
            button7.BackColor = Color.White;
            button8.BackColor = Color.White;
            button9.BackColor = Color.White;
        }

        void rosu()
        {
            int[] produs = new int[8];
            int poz = -1;
            produs[0] = A[0, 0] * A[0, 1] * A[0, 2];
            produs[1] = A[1, 0] * A[1, 1] * A[1, 2];
            produs[2] = A[2, 0] * A[2, 1] * A[2, 2];
            produs[3] = A[0, 0] * A[1, 0] * A[2, 0];
            produs[4] = A[0, 1] * A[1, 1] * A[2, 1];
            produs[5] = A[0, 2] * A[1, 2] * A[2, 2];
            produs[6] = A[0, 0] * A[1, 1] * A[2, 2];
            produs[7] = A[0, 2] * A[1, 1] * A[2, 0];
            for (int i = 0; i < 8; i++)
                if (produs[i] == 1 || produs[i] == 8) poz = i;
            if (poz == 0)
            {
                button1.BackColor = Color.ForestGreen;
                button2.BackColor = Color.ForestGreen;
                button3.BackColor = Color.ForestGreen;
            }
            if (poz == 1)
            {
                button4.BackColor = Color.ForestGreen;
                button5.BackColor = Color.ForestGreen;
                button6.BackColor = Color.ForestGreen;
            }
            if (poz == 2)
            {
                button7.BackColor = Color.ForestGreen;
                button8.BackColor = Color.ForestGreen;
                button9.BackColor = Color.ForestGreen;
            }
            if (poz == 3)
            {
                button1.BackColor = Color.ForestGreen;
                button4.BackColor = Color.ForestGreen;
                button7.BackColor = Color.ForestGreen;
            }
            if (poz == 4)
            {
                button2.BackColor = Color.ForestGreen;
                button5.BackColor = Color.ForestGreen;
                button8.BackColor = Color.ForestGreen;
            }
            if (poz == 5)
            {
                button3.BackColor = Color.ForestGreen;
                button6.BackColor = Color.ForestGreen;
                button9.BackColor = Color.ForestGreen;
            }
            if (poz == 6)
            {
                button1.BackColor = Color.ForestGreen;
                button5.BackColor = Color.ForestGreen;
                button9.BackColor = Color.ForestGreen;
            }
            if (poz == 7)
            {
                button3.BackColor = Color.ForestGreen;
                button5.BackColor = Color.ForestGreen;
                button7.BackColor = Color.ForestGreen;
            }
        }

        char castigator()
        {
            int[] produs = new int[8];
            produs[0] = A[0, 0] * A[0, 1] * A[0, 2];
            produs[1] = A[1, 0] * A[1, 1] * A[1, 2];
            produs[2] = A[2, 0] * A[2, 1] * A[2, 2];
            produs[3] = A[0, 0] * A[1, 0] * A[2, 0];
            produs[4] = A[0, 1] * A[1, 1] * A[2, 1];
            produs[5] = A[0, 2] * A[1, 2] * A[2, 2];
            produs[6] = A[0, 0] * A[1, 1] * A[2, 2];
            produs[7] = A[0, 2] * A[1, 1] * A[2, 0]; 
            for (int i = 0; i < 8; i++)
                if (produs[i] == 1) return 'X';
                else if (produs[i] == 8) return 'O';
            if (pas == 9) return 'E';
            return 'N';
        }

        void muta(int i, int j)
        {
            if (J == 'X')
            {
                J = 'O';
                A[i, j] = 1;
            }
            else
            {
                J = 'X';
                A[i, j] = 2;
            }
            pas++;
            if (castigator() == 'X')
            {
                rosu();
                MessageBox.Show("A castigat jucatorul X!");
                reset();
            }
            else if (castigator() == 'E')
            {
                MessageBox.Show("Egalitate!");
                reset();
            }
            else if (castigator() == 'O')
            {
                rosu();
                MessageBox.Show("A castigat jucatorul O!");
                reset();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            A = new int[3, 3];
            J = 'X';
            label1.Text = label1.Text.Substring(0, 14) + "X";
            pas = 0;
            button1.BackColor = Color.White;
            button2.BackColor = Color.White;
            button3.BackColor = Color.White;
            button4.BackColor = Color.White;
            button5.BackColor = Color.White;
            button6.BackColor = Color.White;
            button7.BackColor = Color.White;
            button8.BackColor = Color.White;
            button9.BackColor = Color.White;
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            button1.Text = J.ToString();
            button1.Enabled = false; 
            muta(0, 0);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Text = J.ToString();
            button2.Enabled = false;
            muta(0, 1);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.Text = J.ToString();
            button3.Enabled = false;
            muta(0, 2);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button4.Text = J.ToString();
            button4.Enabled = false;
            muta(1, 0);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button5.Text = J.ToString();
            button5.Enabled = false;
            muta(1, 1);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button6.Text = J.ToString();
            button6.Enabled = false;
            muta(1, 2);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            button7.Text = J.ToString();
            button7.Enabled = false;
            muta(2, 0);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            button8.Text = J.ToString();
            button8.Enabled = false;
            muta(2, 1);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            button9.Text = J.ToString();
            button9.Enabled = false;
            muta(2, 2);
            label1.Text = label1.Text.Substring(0, 14) + J;
        }

        private void resetareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reset();
            label1.Text = label1.Text.Substring(0, 14) + "X";
        }


    }
}
